Select 
territory_code
,salesrep_name
,ACUTE_CUSTOMER_ACCOUNT_NUMBER 
,ACUTE_CUSTOMER_NAME
,sum(case when rc_flag ='Y' then transitions end) as RC_transitions
,sum(case when rc_flag ='N' then transitions end) as Non_RC_transitions
,sum(transitions) all_Transitions

from (
SELECT A.rpt_month,
       A.month_id,
       A.load_source,
       A.revenue_source,
       A.kci_sales_credit_flag,
       A.billable_flag,
       A.customer_shipto_key,
       A.customer_billto_key,
       A.order_key,
       A.days_in_month,
       A.working_days_in_month,
       A.elapsed_days,
       A.elapsed_working_days,
       A.territory_type,
       A.territory_code,
       A.district,
       A.region,
       A.inside_sales_flag,
       A.customer_shipto_market_category AS ACUTE_MKT_CATEGORY,
       A.transition_facility_name AS ACUTE_CUSTOMER_NAME,
       A.transition_facility_account_number AS ACUTE_CUSTOMER_ACCOUNT_NUMBER,
       A.previous_ro AS ACUTE_RO_NUMBER,
       A.ro_number AS POST_ACUTE_RO_NUMBER,
       A.customer_shipto_market_segment acute_market_seg,
       B.customer_shipto_market_segment post_acute_market_seg,
       B.customer_shipto_market_category AS POST_ACUTE_MKT_CATEGORY,
       B.customer_shipto_account_number AS POST_ACUTE_CUSTOMER_ACCOUNT_NUMBER,
       B.customer_shipto_site_use_id AS POST_ACUTE_SHIPTO_SITE_USE_ID,
       B.customer_shipto_city AS POST_ACUTE_CUSTOMER_CITY,
       CAST(B.customer_shipto_zip_code AS STRING) AS POST_ACUTE_ZIP_CODE,
       CAST(B.customer_shipto_zip4 AS STRING) AS POST_ACUTE_ZIP4,
       CONCAT(CAST(B.customer_shipto_zip_code AS STRING),"-",CAST(B.customer_shipto_zip4 AS STRING)) AS POST_ACUTE_ZIP,
       B.customer_shipto_state AS POST_ACUTE_CUSTOMER_STATE,
       B.customer_shipto_name AS POST_ACUTE_CUSTOMER_NAME,
       B.customer_shipto_indirect_flag,
       B.customer_shipto_class,
       CAST(SUM(A.transitions) AS DECIMAL(38,10)) AS transitions,
       A.product_key,
       A.product_brand,
       A.product_sub_brand,
       'TRANSITIONS' smr_product_group,
       'TRANSITIONS' smr_product_group_updated,
       A.sales_product_group,
       A.product_sku,
       A.product_item_description,
       A.product_family,
       A.customer_shipto_market_segment_desc acute_market_seg_desc,
       B.customer_shipto_market_segment_desc post_acute_market_seg_desc, 
       A.customer_billto_name,
       CASE WHEN A.territory_type IN ('ISR','TSR','ACI','ASI','OSI') THEN 'IST' ELSE 'FST' END team,
       A.national_flag,
       case when a.ready_care_flag ='HOM' and a.rop_type ='VAC' then 'Y' else 'N' end as RC_Flag,
       a.salesrep_name,
       a.rentalsale_flag
FROM /*GET ACUTE INFORMATION FOR TRANSITIONS*/ 
(SELECT f.rpt_month,
       m.month_bgn_date AS month_id,
       f.load_source,
       f.revenue_source,
       f.kci_sales_credit_flag,
       f.billable_flag,
       f.order_key,
       f.acute_order_key,
       f.customer_shipto_key,
       f.customer_billto_key,
       m.x_days_in_month days_in_month,
       m.x_working_days_in_month working_days_in_month,
       m.x_elapsed_days elapsed_days,
       m.x_elapsed_working_days elapsed_working_days,
       f.created_orders,
       f.cancelled_orders,
       f.commissionable_orders,
       f.transitions,
       s.customer_shipto_account_number transition_facility_account_number,
       s.customer_shipto_name transition_facility_name,
       s.customer_shipto_city transition_facility_city,
       s.customer_shipto_zip_code transition_facility_zip_code,
       s.customer_shipto_zip4 transition_facility_zip,
       p.smr_product_group,
       p.product_brand,
       p.product_sub_brand,
       p.product_sku,
       p.product_key,
       p.sales_product_group,
       p.product_item_description,
       p.product_family,
       p.rentalsale_flag,
       o.rop_number,
       o.rop_effective_timestamp,
       o.rop_end_timestamp,
       o.previous_ro,
       o.ro_number,
       o.ro_ship_to_site_use_id,
       o.rop_rec_flag,
       s.customer_shipto_market_category,
       t.region,
       t.district,
       t.territory_code,
       t.geography,
       t.geography_leader nsl_name,
       t.rvp_name,
       t.dm_name,
       t.salesrep_name,
       substr(t.sales_credit_type_code,1,3) territory_type,
       t.inside_sales_flag,
       t.national_flag_org national_flag,
       s.customer_shipto_market_segment,
       s.customer_shipto_market_segment_desc,
       bt.customer_billto_name,
       o.ready_care_flag,
       o.rop_type
FROM aim.aim_ordrev_mthly_snapshot_fact f,
     aim.aim_orders_dim o,
     (SELECT * FROM aim.aim_product_dim WHERE etl_delete_flag = 'N') p,
     (SELECT *,
      (CASE WHEN customer_shipto_market_category='ACUTE' AND sales_credit_Type_code in ('WHS','TMV') THEN 'TRUE'
      WHEN customer_shipto_market_category='HOME' AND sales_credit_Type_code in ('TSV','AWT') THEN 'TRUE'
      WHEN customer_shipto_market_category='EXTENDED' AND sales_credit_Type_code in ('TSV','AWT') THEN 'TRUE'
      WHEN customer_shipto_market_category='EXTENDED' AND sales_credit_Type_code in ('TSR') THEN 'Y'
      WHEN sales_credit_Type_code in ('ASI','OSI','ACI','ISR','ASM') THEN 'NA' 
      ELSE 'FALSE' END) national_flag_org
      FROM aim.aim_customer_zipterr_xref_transposed_vw) t,
     aim.aim_customer_shipto_dim s,
     (SELECT year_month_num,
             month_bgn_date,
             x_elapsed_working_days,
             x_elapsed_days,
             x_working_days_in_month,
             x_days_in_month
      FROM aim.aim_monthday_dim
      GROUP BY 1,2,3,4,5,6) m,
     (SELECT *
      FROM reporting_comops.pulse3_mkt_segment_mapping
      WHERE product = 'TRANSITIONS') b
  LEFT JOIN aim.aim_customer_billto_dim bt ON f.customer_billto_key = bt.customer_billto_key
WHERE f.order_key = o.order_key
AND   f.product_key = p.product_key
AND   f.transition_facility_key = s.customer_shipto_key
AND   s.customer_shipto_key = t.customer_key
AND   m.year_month_num = f.rpt_month
AND   f.rpt_month >= 201801
AND   f.load_source = 'ORDERS'
AND   f.transitions != 0
AND   substr(t.sales_credit_type_code,1,3) NOT IN ('DCS','VCS')
AND   t.region <> 'A0'
AND   s.customer_shipto_market_segment = b.mkt_segment
AND   substr(t.sales_credit_type_code,1,3) = b.territory_type) A

  LEFT JOIN 
  /*GET POST ACUTE DETAILS*/  
  (SELECT customer_shipto_key,
       customer_shipto_market_category,
       customer_shipto_account_number,
       customer_shipto_site_use_id,
       customer_shipto_city,
       customer_shipto_zip_code,
       customer_shipto_zip4,
       customer_shipto_state,
       customer_shipto_indirect_flag,
       customer_shipto_class,
       customer_shipto_name,
       customer_shipto_market_segment,
       customer_shipto_market_segment_desc
FROM aim.aim_customer_shipto_dim) B ON A.customer_shipto_key = B.customer_shipto_key
INNER JOIN (SELECT *
              FROM reporting_comops.pulse3_mkt_segment_mapping
              WHERE product = 'LANDING_FACILITY_TRANSITION_MKT_SEGMENTS') C
          ON A.territory_type = C.territory_type
         AND B.customer_shipto_market_segment = C.mkt_segment
WHERE A.territory_code NOT LIKE '%999%'
GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,38,39
,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54)a
where 
ACUTE_CUSTOMER_ACCOUNT_NUMBER='530895'
and territory_type='ATR'
and rpt_month>=202101 
and rpt_month<=202106 
and rentalsale_flag='R'
group by 1,2,3,4